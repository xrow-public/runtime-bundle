# Runtime Bundle

Runtime features for Ibexa websites.

## Install

```bash
composer config --json extra.symfony.endpoint \
'["https://api.github.com/repos/xrowgmbh/recipes/contents/index.json?ref=main","https://api.github.com/repos/ibexa/recipes/contents/index.json?ref=flex/main","flex://defaults"]' 
```

```bash
composer require xrow/runtime-bundle
```

## Use

```bash
yum install -y php php-pdo php-gd php-sodium php-pecl-swoole5 php-yaml

export APP_RUNTIME="Xrow\Runtime\K8Runtime"

php public/index.php

curl http://localhost:8080
```

## Contribute

```bash
composer req xrow/runtime-bundle:dev-master --prefer-source
```

Create merge request.
