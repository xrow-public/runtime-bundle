<?php

declare(strict_types=1);

namespace Xrow\Runtime;

use Symfony\Component\Runtime\RunnerInterface;
use Runtime\Swoole\Runtime;
use Runtime\Swoole\ServerFactory;
use RenokiCo\PhpK8s\KubernetesCluster;
use Symfony\Component\Process\Process;

class K8Runtime extends Runtime
{
    /** @var ?ServerFactory */
    private $serverFactory;

    public function __construct(array $options, ?ServerFactory $serverFactory = null)
    {
        $options["port"] = 8080;
        $options["namespace"] = 'ibexa-test';

        $options["context"] = '06.xrow.net';
        $options["release"] = 'ibexa';

        $cluster = KubernetesCluster::fromKubeConfigYamlFile(realpath( $_SERVER['HOME'] . '/.kube/config'), $options["context"]);
        $service = $cluster->service()->whereNamespace($options["namespace"])->whereName( $options["release"] . '-mysql' )->get();

        $process = new Process( ['kubectl', 'config', 'use-context', $options["context"], "--namespace=" . $options["namespace"] ] );
        $process->start();
        $process->wait();

        $process = new Process(['kubectl', 'port-forward', 'service/ibexa-mysql', '3306']);
        $process->start();
        $_SERVER['DATABASE_URL'] = 'mysql://ibexa:ibexa@127.0.0.1:3306/ibexa?serverVersion=8&charset=utf8';
        #var_dump($service);

        $this->serverFactory = $serverFactory ?? new ServerFactory($options);
        parent::__construct($this->serverFactory->getOptions());
    }
}