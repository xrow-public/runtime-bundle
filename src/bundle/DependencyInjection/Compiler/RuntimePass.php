<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Xrow\Bundle\Runtime\DependencyInjection\Compiler;

use Ibexa\Bundle\Core\EventListener\RejectExplicitFrontControllerRequestsListener;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RuntimePass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(RejectExplicitFrontControllerRequestsListener::class)) {
            return;
        }
        $container->removeDefinition(RejectExplicitFrontControllerRequestsListener::class);
    }
}